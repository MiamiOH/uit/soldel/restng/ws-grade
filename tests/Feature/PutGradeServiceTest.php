<?php
/**
 * Created by PhpStorm.
 * User: duhy
 * Date: 7/28/18
 * Time: 1:17 PM
 */

namespace MiamiOH\GradeWebService\Tests\Feature;

use MiamiOH\Pike\Domain\Collection\UpdateGradeResponseCollection;
use MiamiOH\Pike\Domain\ValueObject\UpdateGradeResponse;
use MiamiOH\RESTng\App;

class PutGradeServiceTest extends TestCase
{
    private $gradePut;

    private $gradePut1;

    private $gradePut2;

    public function testPutGradeInvalidGradeTypeWithAdminUser()
    {
        $this->gradePut = new UpdateGradeResponse(
            0,
            false,
            'Invalid grade type, can only accept midterm and final.'
        );

        $this->user->method('getUsername')->willReturn('liaom');
        $this->user->method('checkAuthorization')->willReturn('admin');
        $this->authorizationValidator->method('validateAuthorizationForKey')->willReturn(true);

        $this->updateGradeService
            ->method('bulkUpdate')
            ->willReturn(new UpdateGradeResponseCollection(
                [$this->gradePut]
            ));

        $response = $this->putJson('/grade/v2/grade?token=liaom', [
            [   'courseSectionGuid' => (string)$this->guid,
                'uniqueId' => 'liaom',
                'gradeType' => 'fefwefew',
                'grade' => 'A'
            ]
        ]);

        $response->assertStatus(App::API_OK);
        $response->assertJson([
                'data' => [
                    [
                        'isSuccessful' => false,
                        'message' => 'Invalid grade type, can only accept midterm and final.'
                    ]
                ]
            ]
        );
    }

    public function testPutGradeInvalidCourseSectionGuidWithAdminUser()
    {
        $this->gradePut = new UpdateGradeResponse(
            0,
            false,
            'Invalid course section GUID'
        );

        $this->user->method('getUsername')->willReturn('liaom');
        $this->user->method('checkAuthorization')->willReturn('admin');
        $this->authorizationValidator->method('validateAuthorizationForKey')->willReturn(true);

        $this->updateGradeService
            ->method('bulkUpdate')
            ->willReturn(new UpdateGradeResponseCollection(
                [$this->gradePut]
            ));

        $response = $this->putJson('/grade/v2/grade?token=liaom', [
            [   'courseSectionGuid' => 'esfssfsefesfesfs',
                'uniqueId' => 'liaom',
                'gradeType' => 'fefwefew',
                'grade' => 'A'
            ]
        ]);

        $response->assertStatus(App::API_OK);
        $response->assertJson([
                'data' => [
                    [
                        'isSuccessful' => false,
                        'message' => 'Invalid course section GUID'
                    ]
                ]
            ]
        );
    }

    public function testPutGradeWithAdminUser()
    {
        $this->gradePut1 = new UpdateGradeResponse(
            0,
            true,
            'Is Successful'
        );

        $this->user->method('getUsername')->willReturn('xiaw');
        $this->user->method('checkAuthorization')->willReturn('admin');
        $this->authorizationValidator->method('validateAuthorizationForKey')->willReturn(true);

        $this->updateGradeService
            ->method('bulkUpdate')
            ->willReturn(new UpdateGradeResponseCollection(
                [$this->gradePut1]
            ));

        $response = $this->putJson('/grade/v2/grade?token=xiaw', [
            [   'courseSectionGuid' => (string)$this->guid,
                'uniqueId' => 'liaom',
                'gradeType' => 'midterm',
                'grade' => 'A'
            ]
        ]);

        $response->assertStatus(App::API_OK);
        $response->assertJson([
                'data' => [
                    [
                        'isSuccessful' => true,
                        'message' => 'Is Successful'
                    ]
                ]
            ]
        );
    }

    public function testPutGradeWithStandardUser()
    {
        $this->gradePut2 = new UpdateGradeResponse(
            0,
            false,
            'You are unauthorized to update students\' grades in this course section'
        );

        $this->user->method('getUsername')->willReturn('liaom');
        $this->user->method('checkAuthorization')->willReturn('null');
        $this->authorizationValidator->method('validateAuthorizationForKey')->willReturn(true);

        $this->updateGradeService
            ->method('bulkUpdate')
            ->willReturn(new UpdateGradeResponseCollection(
                [$this->gradePut2]
            ));

        $response = $this->putJson('/grade/v2/grade?token=xiaw', [
            [   'courseSectionGuid' => (string)$this->guid,
                'uniqueId' => 'liaom',
                'gradeType' => 'midterm',
                'grade' => 'A'
            ]
        ]);

        $response->assertStatus(App::API_OK);
        $response->assertJson([
                'data' => [
                    [
                        'isSuccessful' => false,
                        'message' => 'You are unauthorized to update students\' grades in this course section'
                    ]
                ]
            ]
        );
    }
}
