<?php

namespace MiamiOH\GradeWebService\Services;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Service;

class BannerBase extends Service
{
    public $database;
    public $dataSource      = '';
    private $configuration   = '';
    private $datasource_name = 'MUWS_SEC_PROD';
    //	public $datasource_name = 'MUWS_GEN_PROD';
    public $dbh;

    public function setDataSource($datasource)
    {
        $this->dataSource = $datasource;
    }

    public function setDatabase($database)
    {
        $this->database = $database;
        $this->dbh = $database->getHandle($this->datasource_name);
    }

    public function setConfiguration($configuration)
    {
        $this->configuration = $configuration;
    }

    public function getDatabaseHandle()
    {
        return $this->dbh;
        //		return $this->database->getHandle($this->datasource_name);
    }

    public function getPidm($username)
    {
        $sql = 'select szbuniq_pidm from SATURN.szbuniq where szbuniq_unique_id=:USERNAME';
        $vars = array('USERNAME' => strtoupper($username));
        return $this->bindquery_select($sql, $vars);
    }

    // Response Methods

    public function unauthorizedRequest()
    {
        $response = $this->getResponse();
        $response->setStatus(App::API_UNAUTHORIZED);
        return $response;
    }

    public function noData()
    {
        $response = $this->getResponse();
        $response->setStatus(App::API_NOTFOUND);
        return $response;
    }

    public function updatedResponse($data)
    {
        $response = $this->getResponse();
        $response->setStatus(App::API_CREATED);
        $response->setPayload($data);
        return $response;
    }

    public function makeCollectionResponse($data)
    {
        $response = $this->getResponse();
        $response->setStatus(App::API_OK);
        $response->setPayload($data);
        return $response;
    }

    public function makeSingleResponse($data)
    {
        if (!$data || !is_array($data) || count($data) < 1) {
            return $this->noData();
        }

        $response = $this->getResponse();
        $response->setStatus(App::API_OK);
        $response->setPayload($data);
        return $response;
    }

    // Database Query Methods

    public function bindquery($sql, &$vars)
    {
        $dbh = $this->getDatabaseHandle();
        $sth = $dbh->prepare($sql);
        if ($vars) {
            foreach ($vars as $var=>&$val) {
                $sth->bind_by_name($var, $val);
            }
        }
        $sth->execute();
        return $sth;
    }

    public function bindexecute($sql, &$vars)
    {
        $sth = $this->bindquery($sql, $vars);
    }

    public function bindqueryall_assoc($sql, &$vars)
    {
        $sth = $this->bindquery($sql, $vars);
        $records = array();
        while ($record = $sth->fetchrow_assoc()) {
            $records[] = $record;
        }
        return $records;
    }

    public function bindquerysinglecol($sql, &$vars=array())
    {
        return $this->getDatabaseHandle()->queryall_list($sql);
    }

    public function bindqueryrow_assoc($sql, &$vars)
    {
        $sth = $this->bindquery($sql, $vars);
        return $sth->fetchrow_assoc();
    }

    public function bindquery_select($sql, &$vars=array())
    {
        $sth = $this->bindquery($sql, $vars);
        $row = $sth->fetchrow_array();
        return $row[0];
    }

    public function bindquery_field($sql, $field, &$vars=array())
    {
        $sth = $this->bindquery($sql, $vars);
        $row = $sth->fetchrow_assoc();
        return $row[$field];
    }

    // Privilege / Access Methods

    // Returns I=instructor, S=student, SI=student/instrctor, empty string =neither
    public function getRole($pidm, $term='')
    {
        if ($term) {
            $iwhere = 'AND sirasgn_term_code=:TERM_ID';
            $swhere = 'AND sfrstcr_term_code=:TERM_ID';
            $vars['TERM_ID'] = $term;
        } else {
            $iwhere = $swhere = '';
        }

        $sql =
            "SELECT NVL(S.PRIVS,'')||NVL(I.PRIVS,'') myprivs FROM 
(SELECT 1 KEY FROM DUAL) D,
(SELECT 1 KEY, 'I' PRIVS FROM sirasgn f WHERE sirasgn_pidm=:PIDM $iwhere AND ROWNUM<2) I,
(SELECT 1 KEY, 'S' PRIVS FROM sfrstcr f WHERE sfrstcr_pidm=:PIDM $swhere AND ROWNUM<2) S
WHERE D.KEY=I.KEY(+) AND D.KEY=S.KEY(+)";

        $vars['PIDM'] = $pidm;

        return $this->bindquery_field($sql, 'myprivs', $vars);
    }

    public function checkAuthorization($role)
    {
        $a = $this->getApiUser()->checkAuthorization(
            array(
                'type' => 'authMan',
                'application' => 'WebServices',
                'module' => 'EnrollmentService',
                'key' => array($role)
            )
        );

        if (isset($this->error) && $this->error) {
            return false;
        } elseif (!$a) {
            return false;
        }
        return true;
    }

    public function checkAuth()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $options = $request->getOptions();
        $role = isset($options['role']) ? $options['role'] : 'all';
        $special = isset($options['specialUser']) ? $options['specialUser'] : 'N';

        $user = $this->getApiUser();

        $payload = array();

        $payload['username'] = $user->getUsername();
        $payload['enrollmentRole'] = $role;
        $payload['specialUser'] = $special;

        $payload['authorizedAllContext'] = $user->checkAuthorization(
            array(
                'type' => 'authMan',
                'application' => 'WebServices',
                'module' => 'EnrollmentService',
                'key' => array('All')
            )
        );

        $payload['authorizedInstructorContext'] = $user->checkAuthorization(
            array(
                'type' => 'authMan',
                'application' => 'WebServices',
                'module' => 'EnrollmentService',
                'key' => array('Instructor')
            )
        );

        $payload['authorizedStudentContext'] = $user->checkAuthorization(
            array(
                'type' => 'authMan',
                'application' => 'WebServices',
                'module' => 'EnrollmentService',
                'key' => array('Student')
            )
        );

        $response->setStatus(App::API_OK);
        $response->setPayload($payload);

        return $response;
    }

    public function isSpecialUser()
    {
        $options = $this->getRequest()->getOptions();
        return isset($options['specialUser']) && $options['specialUser'] == 'Y';
    }

    public function accessOK($myuserid, $pidm, $userid, $termid='', $myrole='')
    {
        $options = $this->getRequest()->getOptions();
        $role = isset($options['role']) ? $options['role'] : 'all';

        if ($this->isSpecialUser()) {
            return $this->checkAuthorization($role);
        }

        if (!$myrole) {
            $myrole = $this->getRole($pidm, $termid);
        }

        if (!$myrole) {		// You are not in the database
            return false;
        }
        if ($myrole=='S' && $userid && strcasecmp($userid, $myuserid)) {	// Students may not query anyoe but themself
            return false;
        }
        return true;
    }

    // Resource Methods

    public function academicTermResource($termid)
    {
        return $this->locationFor('grade.test.v2.academicTerm', array('termId' => $termid));
    }

    public function enrollmentResource($termid, $user)
    {
        return $this->locationFor('grade.test.v2.enrollmentsUserTerm', array('termId' => $termid, 'username' => $user));
    }

    public function enrollmentResourceCourse($termid, $courseid, $user)
    {
        return $this->locationFor('grade.test.v2.enrollmentsUserTermCourse', array('termId' => $termid, 'crn' => $courseid, 'username' => $user));
    }

    public function enrollmentTermResource($termid, $user)
    {
        return $this->locationFor('grade.test.v2.userEnrollmentTermsByTerm', array('termId' => $termid, 'username' => $user));
    }

    public function sectionResource($termid, $courseid)
    {
        return "http://wsdev.miamioh.edu/courseSectionV2/$termid/$courseid";
        //return $this->locationFor('grade.test.v2.courseSection', array('termId' => $termid, 'courseId' => $courseid));
    }

    public function personResource($user)
    {
        return "http://wsdev.miamioh.edu/person/$user";
        //return $this->locationFor('grade.test.person', array('username' => $user));
    }
}
