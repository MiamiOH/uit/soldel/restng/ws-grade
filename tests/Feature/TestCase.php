<?php
/**
 * Created by PhpStorm.
 * User: duhy
 * Date: 7/25/18
 * Time: 4:18 PM
 */

namespace MiamiOH\GradeWebService\Tests\Feature;
use MiamiOH\Pike\App\Framework\RESTng\PikeServiceFactory;
use MiamiOH\Pike\App\Mapper\AppMapper;
use MiamiOH\Pike\Domain\Model\Grade;
use MiamiOH\Pike\Domain\Model\InstructorAssignment;
use MiamiOH\Pike\Domain\ValueObject\Crn;
use MiamiOH\Pike\Domain\ValueObject\GradeModeCode;
use MiamiOH\Pike\Domain\ValueObject\GradeType;
use MiamiOH\Pike\Domain\ValueObject\GradeValue;
use MiamiOH\Pike\Domain\ValueObject\TermCode;
use MiamiOH\Pike\Domain\ValueObject\UniqueId;
use MiamiOH\Pike\App\Service\ViewGradeService;
use MiamiOH\Pike\App\Service\ViewInstructorAssignmentService;
use MiamiOH\Pike\App\Service\UpdateGradeService;
use MiamiOH\Pike\Domain\ValueObject\CourseSectionGuid;
use MiamiOH\Pike\Domain\ValueObject\InstructorAssignmentGuid;
use MiamiOH\RESTng\Util\User;
use PHPUnit\Framework\MockObject\MockObject;

class TestCase extends \MiamiOH\RESTng\Testing\TestCase
{
    /**
     * @var MockObject
     */
    protected $viewGradeService;

    protected $viewInstructorAssignmentService;

    protected $updateGradeService;

    protected $credentialValidator;

    protected $authorizationValidator;

    protected $user;

    protected $guid;

    protected $instructorGuid;

    protected function setUp()
    {
        parent::setUp();

        $pikeServiceFactory = $this->createMock(PikeServiceFactory::class);
        $this->viewGradeService = $this->createMock(ViewGradeService::class);
        $this->viewInstructorAssignmentService = $this->createMock(ViewInstructorAssignmentService::class);
        $this->updateGradeService = $this->createMock(UpdateGradeService::class);

        $pike = $this->createMock(AppMapper::class);
        $pike->method('getViewGradeService')->willReturn($this->viewGradeService);
        $pike->method('getViewInstructorAssignmentService')->willReturn($this->viewInstructorAssignmentService);
        $pike->method('getUpdateGradeService')->willReturn($this->updateGradeService);

        $pikeServiceFactory->method('getEloquentPikeServiceMapper')->willReturn($pike);

        $this->app->useService([
            'name' => 'PikeServiceFactory',
            'object' => $pikeServiceFactory,
            'description' => 'Mocked Pike Service Factory'
        ]);

        $this->user = $this->createMock(User::class);

        $this->credentialValidator =
            $this->createMock(\MiamiOH\RESTng\Service\CredentialValidator\CredentialValidatorInterface::class);

        $this->app->useService([
            'name' => 'CredentialValidator',
            'object' => $this->credentialValidator,
            'description' => 'Mock credential validator',
        ]);

        $this->credentialValidator->method('validateToken')->willReturn([
            'valid' => true,
            'username' => 'liaom',
        ]);

        $this->authorizationValidator =
            $this->createMock(\MiamiOH\RESTng\Service\AuthorizationValidator\AuthorizationValidatorInterface::class);

        $this->app->useService([
            'name' => 'AuthorizationValidator',
            'object' => $this->authorizationValidator,
            'description' => 'Mock authorization validator',
        ]);

        $this->guid = CourseSectionGuid::create();
        $this->instructorGuid = InstructorAssignmentGuid::create();
    }

    protected function mockGrade(
        string $value,
        string $mode,
        string $type,
        string $guiId,
        string $crn,
        string $termCode,
        string $uniqueId
    ): MockObject {
        $grade = $this->createMock(Grade::class);
        $grade->method('getValue')->willReturn(new GradeValue($value));
        $grade->method('getGradeModeCode')->willReturn(new GradeModeCode($mode));
        $grade->method('getType')->willReturn(new GradeType($type));
        $grade->method('getCourseSectionGuid')->willReturn($this->guid);
        $grade->method('getCrn')->willReturn(new Crn($crn));
        $grade->method('getTermCode')->willReturn(new TermCode($termCode));
        $grade->method('getUniqueId')->willReturn(new UniqueId($uniqueId));

        return $grade;
    }

    protected function mockInstructor(
        string $InstructorAssignmentGuid,
        string $uniqueId,
        string $termCode,
        string $crn,
        string $guiId,
        string $isPrimary
    ): MockObject {
        $instructor = $this->createMock(InstructorAssignment::class);
        $instructor->method('getGuid')->willReturn($this->instructorGuid);
        $instructor->method('getUniqueId')->willReturn(new UniqueId($uniqueId));
        $instructor->method('getTermCode')->willReturn(new TermCode($termCode));
        $instructor->method('getCrn')->willReturn(new Crn($crn));
        $instructor->method('getCourseSectionGuid')->willReturn($this->guid);
        $instructor->method('isPrimary')->willReturn($isPrimary);

        return $instructor;
    }
}
