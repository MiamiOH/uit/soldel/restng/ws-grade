# Grade RESTful Web Service

## Description

ws-grade is implemented under RESTng framework and used to provide information of 
students' grade. It was originally written in JaxRS (javaapps-BannerWebServices) 
and then rewritten in RESTng (projects-bannerREST). Currently, team EAST rewrote it
again in order to providing more stable solution.

## API Documentation

API documentation can be found on swagger page: <ws_url>/api/swagger-ui/#/grade

## Local Development Setup

1. pull down latest source code from this repository
2. install composer dependencies: `composer update`
3. make sure `pike` has installed as an addon of `restng`
4. make symlink in the `restng` folder (make sure environment variable `$RESTNG_HOME` is set)

```bash
ln -s <path_to_ws-grade>/src/RESTconfig $RESTNG_HOME/RESTconfig/grade
ln -s <path_to_ws-grade>/src/Service $RESTNG_HOME/Service/Grade
```

## Testing

### Unit Testing

Unit test cases in this project is written using PHPUnit.

`phpunit` should pass without any error message before and after making any change. Code coverage report will be
automatically generated after `phpunit` being ran and put into `test/coverage` folder.

## Usage

### Get Grade

#### Resources (GET: /grade/v2/grade):

##### Description:
This resource provides get grades function to the consumer application. One grade can be get either by using term code 
and crn or courseSectionGuid.

##### query option
- termCode :term code
- crn :course crn
- uniqueId :student's UniqueID that you want to get grade for. It must be a valid Miami UniqueID
- courseSectionGuid : a valid course section GUID from SATURN.SSBSGID table.
- type : either 'midterm' or 'final'

##### Response Body:
A collection of grade object in the request body
and contains following attributes:
- termCode :term code
- crn :course crn
- uniqueId :student's UniqueID that you want to get grade for. It must be a valid Miami UniqueID
- courseSectionGuid : a valid course section GUID from SATURN.SSBSGID table.
- type : either 'midterm' or 'final'
- value: grade value

##### Authentication:

Consumer must provide a valid Miami WS token

##### Authorization:

- With admin access, consumers are able to get any grade. Contact Solution Delivery
Support to add your UniqueID with key `admin` into AuthMan (application: `WebServices`, module: `GradeService`).
- Consumers are ONLY able to get grades of students they taught or students themself.

### Update Grade

#### Resources (PUT: /grade/v2/grade):

##### Description:
This resource provides bulk update grades function to the consumer application. Each
request will be proceed individually. Errors happened during update process will not
stop the whole process. Instead, they will be logged into response body and return to 
consumer after process end.

##### Authentication:
Consumer must provide a valid Miami WS token

##### Authorization:
- With admin access, consumers are able to update any grade. Contact Solution Delivery
Support to add your UniqueID with key `admin` into AuthMan (application: `WebServices`
, module: `GradeService`).
- Consumers are ONLY able to update grades of students they taught.

##### Request Body:
A collection of requests which contains following attributes:
- courseSectionGuid (required): a valid course section GUID from SATURN.SSBSGID table.
- uniqueId (required): student's UniqueID that you want to update grade for. It must 
be a valid Miami UniqueID
- gradeType (required): either 'midterm' or 'final'
- grade (required): a valid grade value. e.g. A, B+, etc.
- hasAttended (optional): either true or false
- lastAttendDate (optional): a valid date in YYYY-MM-DD format. e.g. 2018-03-31

##### Response Body:
A collection of update status which matches the order of requests in the request body
and contains following attributes:
- isSuccessful: either true or false
- message: message is always "Ok" if grade update successfully. Otherwise, message 
can be vary which depend on error happened during update process
