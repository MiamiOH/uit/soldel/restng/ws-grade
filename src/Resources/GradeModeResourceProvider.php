<?php

namespace MiamiOH\GradeWebService\Resources;

use MiamiOH\RESTng\App;

class GradeModeResourceProvider extends \MiamiOH\RESTng\Util\ResourceProvider
{
    public function registerDefinitions(): void
    {
        $this->addTag(array(
            'name' => 'grade',
            'description' => 'Resources for providing grade values'
        ));

        $this->addDefinition(array(
            'name' => 'Academic.gradeMode',
            'type' => 'object',
            'properties' => array(
                'gradeModeId' => array(
                    'type' => 'string',
                ),
                'gradeModeName' => array(
                    'type' => 'string',
                ),
                'gradeLevel' => array(
                    'type' => 'string',
                ),
                'gradeValue' => array(
                    'type' => 'string',
                ),
                'gradeName' => array(
                    'type' => 'string',
                ),
                'passingGrade' => array(
                    'type' => 'boolean',
                ),
                'attendanceInformation' => array(
                    'type' => 'string',
                ),
                'gradeType' => array(
                    'type' => 'string',
                )
            )
        ));

        $this->addDefinition(array(
            'name' => 'Academic.gradeModeValue',
            'type' => 'object',
            'properties' => array(

                'gradeModeId' => array(
                    'type' => 'string',
                ),
                'gradeModeName' => array(
                    'type' => 'string',
                ),
                'gradeLevel' => array(
                    'type' => 'string',
                )
            )
        ));

        $this->addDefinition(array(
            'name' => 'Academic.GradeModeTranslation',
            'type' => 'object',
            'properties' => array(
                'gradeLevel' => array(
                    'type' => 'string',
                ),
                'gradeModeId' => array(
                    'type' => 'string',
                ),
                'gradeModeName' => array(
                    'type' => 'string',
                ),
                'externalGradeMode' => array(
                    'type' => 'string',
                ),
                'externalGradeModeName' => array(
                    'type' => 'string',
                ),
                'externalGrade' => array(
                    'type' => 'string',
                ),
                'gradeValue' => array(
                    'type' => 'string',
                )
            )
        ));

        $this->addDefinition(array(
            'name' => 'Academic.GradeModeTranslation.Collection',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/academic.GradeModeTranslation'
            )
        ));


        $this->addDefinition(array(
            'name' => 'Academic.gradeMode.Collection',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/academic.gradeMode'
            )
        ));
    }

    public function registerServices(): void
    {
        $this->addService(array(
            'name' => 'Grade.Test.gradeMode',
            'class' => 'MiamiOH\GradeWebService\Services\GradeModeService',
            'description' => 'Grade modes',
            'set'  => array(
                'database'      => array('type' => 'service', 'name' => 'APIDatabaseFactory'),
                'configuration' => array('type' => 'service', 'name' => 'APIConfiguration'),
                'dataSource'    => array('type' => 'service', 'name' => 'APIDataSourceFactory'),
            ),
        ));
    }

    public function registerResources(): void
    {
        $this->addResource(array(
            'action' => 'read',
            'name' => 'grade.test.v2.gradeMode',
            'description' => 'get a collection of grade modes.',
            'summary' => 'get a collection of grade modes.',
            'pattern' => '/grade/v2/gradeMode',
            'service' => 'Grade.Test.gradeMode',
            'tags' => array('grade'),
            'method' => 'getGradeModes',
            'returnType' => 'collection',
            'responses' => array(
                App::API_OK => array(
                    'description' => 'A list of grade modes',
                    'returns' => array(
                        'type' => 'array',
                        '$ref' => '#/definitions/Academic.gradeModeValue',
                    )
                ),
            )
        ));

        $this->addResource(array(
            'action' => 'read',
            'name' => 'grade.test.v2.gradeModeLevel',
            'description' => 'get a collection of grade modes by grade level.',
            'summary' => 'get a collection of grade modes.',
            'pattern' => '/grade/v2/gradeMode/:gradeLevel',
            'service' => 'Grade.Test.gradeMode',
            'tags' => array('grade'),
            'method' => 'getGradeModes',
            'returnType' => 'model',
            'params' => array(
                'gradeLevel' => array('description' => 'grade level'),
            ),
            'responses' => array(
                App::API_OK => array(
                    'description' => 'A grade mode',
                    'returns' => array(
                        'type' => 'object',
                        '$ref' => '#/definitions/Academic.gradeModeValue',
                    )
                ),
            )
        ));

        $this->addResource(array(
            'action' => 'read',
            'name' => 'grade.test.v2.gradeModeLevelMode',
            'description' => 'get a collection of grade modes by grade level and grade mode.',
            'summary' => 'get a collection of grade modes.',
            'pattern' => '/grade/v2/gradeMode/:gradeLevel/:gradeMode',
            'service' => 'Grade.Test.gradeMode',
            'tags' => array('grade'),
            'method' => 'getGradeModeID',
            'returnType' => 'model',
            'params' => array(
                'gradeLevel' => array('description' => 'grade level'),
                'gradeMode' => array('description' => 'grade mode'),
            ),
            'responses' => array(
                App::API_OK => array(
                    'description' => 'A grade mode',
                    'returns' => array(
                        'type' => 'object',
                        '$ref' => '#/definitions/Academic.gradeModeValue',
                    )
                ),
            )
        ));

        $this->addResource(array(
            'action' => 'read',
            'name' => 'grade.test.v2.gradeModeValue',
            'description' => 'get a collection of grade mode values.',
            'pattern' => '/grade/v2/gradeModeValue',
            'service' => 'Grade.Test.gradeMode',
            'tags' => array('grade'),
            'method' => 'getGradeModeValues',
            'returnType' => 'collection',
            'responses' => array(
                App::API_OK => array(
                    'description' => 'A list of grade mode values',
                    'returns' => array(
                        'type' => 'array',
                        '$ref' => '#/definitions/Academic.gradeMode',
                    )
                ),
            )
        ));

        $this->addResource(array(
            'action' => 'read',
            'name' => 'grade.test.v2.gradeModeValue.Level',
            'description' => 'get a collection of grade mode values by grade level.',
            'summary' => 'get a collection of grade mode values.',
            'pattern' => '/grade/v2/gradeModeValue/:gradeLevel',
            'service' => 'Grade.Test.gradeMode',
            'tags' => array('grade'),
            'method' => 'getGradeModeValues',
            'returnType' => 'collection',
            'params' => array(
                'gradeLevel' => array('description' => 'grade level'),
            ),
            'responses' => array(
                App::API_OK => array(
                    'description' => 'A list of grade modes',
                    'returns' => array(
                        'type' => 'array',
                        '$ref' => '#/definitions/Academic.gradeMode',
                    )
                ),
            )
        ));

        $this->addResource(array(
            'action' => 'read',
            'name' => 'grade.test.v2.gradeModeValue.LevelId',
            'description' => 'get a collection of grade mode values by grade level and grade mode ID.',
            'summary' => 'get a collection of grade mode values.',
            'pattern' => '/grade/v2/gradeModeValue/:gradeLevel/:gradeModeId',
            'service' => 'Grade.Test.gradeMode',
            'tags' => array('grade'),
            'method' => 'getGradeModeValues',
            'returnType' => 'collection',
            'params' => array(
                'gradeLevel' => array('description' => 'grade level'),
                'gradeModeId' => array('description' => 'grade mode id'),
            ),
            'responses' => array(
                App::API_OK => array(
                    'description' => 'A list of grade modes',
                    'returns' => array(
                        'type' => 'array',
                        '$ref' => '#/definitions/Academic.gradeMode',
                    )
                ),
            )
        ));

        $this->addResource(array(
            'action' => 'read',
            'name' => 'grade.test.v2.gradeModeValue.LevelIdType',
            'description' => 'get a collection of grade mode values by grade level, grade mode ID, and grade type.',
            'summary' => 'get a collection of grade mode values.',
            'pattern' => '/grade/v2/gradeModeValue/:gradeLevel/:gradeModeId/:gradeType',
            'service' => 'Grade.Test.gradeMode',
            'tags' => array('grade'),
            'method' => 'getGradeModeValues',
            'returnType' => 'collection',
            'params' => array(
                'gradeLevel' => array('description' => 'grade level'),
                'gradeModeId' => array('description' => 'grade mode id'),
                'gradeType' => array('description' => 'grade type', 'enum' => ['midterm', 'final']),
            ),
            'responses' => array(
                App::API_OK => array(
                    'description' => 'A list of grade modes',
                    'returns' => array(
                        'type' => 'array',
                        '$ref' => '#/definitions/Academic.gradeMode',
                    )
                ),
            )
        ));

        $this->addResource(array(
            'action' => 'read',
            'name' => 'grade.test.v2.gradeModeValue.LevelIdTypeVal',
            'description' => 'get a collection of grade mode values by grade level, grade mode ID, grade type, and grade value.',
            'summary' => 'get a collection of grade mode values.',
            'pattern' => '/grade/v2/gradeModeValue/:gradeLevel/:gradeModeId/:gradeType/:gradeValue',
            'service' => 'Grade.Test.gradeMode',
            'tags' => array('grade'),
            'method' => 'getGradeModeValueID',
            'returnType' => 'collection',
            'params' => array(
                'gradeLevel' => array('description' => 'grade level'),
                'gradeModeId' => array('description' => 'grade mode id'),
                'gradeType' => array('description' => 'grade type', 'enum' => ['midterm', 'final']),
                'gradeValue' => array('description' => 'grade value'),
            ),
            'responses' => array(
                App::API_OK => array(
                    'description' => 'A list of grade modes',
                    'returns' => array(
                        'type' => 'array',
                        '$ref' => '#/definitions/Academic.gradeMode',
                    )
                ),
            )
        ));

        $this->addResource(array(
            'action' => 'read',
            'name' => 'grade.test.v2.gradeModeTranslation',
            'description' => 'get a collection of grade mode translations.',
            'pattern' => '/grade/v2/gradeModeTranslation',
            'service' => 'Grade.Test.gradeMode',
            'tags' => array('grade'),
            'method' => 'getGradeModeTranslations',
            'returnType' => 'collection',
            'responses' => array(
                App::API_OK => array(
                    'description' => 'A list of grade mode translations',
                    'returns' => array(
                        'type' => 'array',
                        '$ref' => '#/definitions/Academic.GradeModeTranslation',
                    )
                ),
            )
        ));

        $this->addResource(array(
            'action' => 'read',
            'name' => 'grade.test.v2.gradeModeTranslation.Level',
            'description' => 'get a collection of grade mode translations by grade level.',
            'summary' => 'get a collection of grade mode translations.',
            'pattern' => '/grade/v2/gradeModeTranslation/:gradeLevel',
            'service' => 'Grade.Test.gradeMode',
            'tags' => array('grade'),
            'method' => 'getGradeModeTranslations',
            'returnType' => 'collection',
            'params' => array(
                'gradeLevel' => array('description' => 'grade level'),
            ),
            'responses' => array(
                App::API_OK => array(
                    'description' => 'A list of grade mode translations',
                    'returns' => array(
                        'type' => 'array',
                        '$ref' => '#/definitions/Academic.GradeModeTranslation',
                    )
                ),
            )
        ));

        $this->addResource(array(
            'action' => 'read',
            'name' => 'grade.test.v2.gradeModeTranslation.LevelMode',
            'description' => 'get a collection of grade mode translations by grade level and grade mode.',
            'summary' => 'get a collection of grade mode translations.',
            'pattern' => '/grade/v2/gradeModeTranslation/:gradeLevel/:gradeMode',
            'service' => 'Grade.Test.gradeMode',
            'tags' => array('grade'),
            'method' => 'getGradeModeTranslations',
            'returnType' => 'collection',
            'params' => array(
                'gradeLevel' => array('description' => 'grade level'),
                'gradeMode' => array('description' => 'grade mode'),
            ),
            'responses' => array(
                App::API_OK => array(
                    'description' => 'A list of grade mode translations',
                    'returns' => array(
                        'type' => 'array',
                        '$ref' => '#/definitions/Academic.GradeModeTranslation',
                    )
                ),
            )
        ));

        $this->addResource(array(
            'action' => 'read',
            'name' => 'grade.test.v2.gradeModeTranslation.ExtGradeMode',
            'description' => 'get a collection of grade mode translations by grade level, grade mode, and external grade mode.',
            'summary' => 'get a collection of grade mode translations.',
            'pattern' => '/grade/v2/gradeModeTranslation/:gradeLevel/:gradeMode/:externalGradeMode',
            'service' => 'Grade.Test.gradeMode',
            'tags' => array('grade'),
            'method' => 'getGradeModeTranslations',
            'returnType' => 'collection',
            'params' => array(
                'gradeLevel' => array('description' => 'grade level'),
                'gradeMode' => array('description' => 'grade mode'),
                'externalGradeMode' => array('description' => 'external grade mode'),
            ),
            'responses' => array(
                App::API_OK => array(
                    'description' => 'A list of grade mode translations',
                    'returns' => array(
                        'type' => 'array',
                        '$ref' => '#/definitions/Academic.GradeModeTranslation',
                    )
                ),
            )
        ));

        $this->addResource(array(
            'action' => 'read',
            'name' => 'grade.test.v2.gradeModeTranslation.ExtGrade',
            'description' => 'get a collection of grade mode translations by grade level, grade mode, external grade mode, and external grade.',
            'summary' => 'get a collection of grade mode translations.',
            'pattern' => '/grade/v2/gradeModeTranslation/:gradeLevel/:gradeMode/:externalGradeMode/:externalGrade',
            'service' => 'Grade.Test.gradeMode',
            'tags' => array('grade'),
            'method' => 'getGradeModeTranslationID',
            'returnType' => 'collection',
            'params' => array(
                'gradeLevel' => array('description' => 'grade level'),
                'gradeMode' => array('description' => 'grade mode'),
                'externalGradeMode' => array('description' => 'external grade mode'),
                'externalGrade' => array('description' => 'external grade'),
            ),
            'responses' => array(
                App::API_OK => array(
                    'description' => 'A list of grade mode translations',
                    'returns' => array(
                        'type' => 'array',
                        '$ref' => '#/definitions/Academic.GradeModeTranslation',
                    )
                ),
            )
        ));
    }

    public function registerOrmConnections(): void
    {
    }
}
