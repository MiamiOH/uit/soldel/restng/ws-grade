<?php

return [
    'resources' => [
        'grade' => [
            MiamiOH\GradeWebService\Resources\GradeResourceProvider::class,
            MiamiOH\GradeWebService\Resources\GradeModeResourceProvider::class
        ]
    ]
];