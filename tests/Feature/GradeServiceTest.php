<?php
/**
 * Created by PhpStorm.
 * User: duhy
 * Date: 7/24/18
 * Time: 3:39 PM
 */

namespace MiamiOH\GradeWebService\Tests\Feature;

use MiamiOH\Pike\Domain\Collection\GradeCollection;
use MiamiOH\Pike\Domain\Collection\InstructorAssignmentCollection;
use MiamiOH\Pike\Exception\InvalidCourseSectionGuidException;
use MiamiOH\Pike\Exception\InvalidCrnException;
use MiamiOH\Pike\Exception\InvalidGradeTypeException;
use MiamiOH\Pike\Exception\InvalidTermCodeException;
use MiamiOH\Pike\Exception\InvalidUniqueIdException;
use MiamiOH\RESTng\App;


class GradeServiceTest extends TestCase
{
    public function testInvalidTermCode()
    {
        $this->user->method('getUsername')->willReturn('liaom');
        $this->user->method('checkAuthorization')->willReturn('admin');
        $this->authorizationValidator->method('validateAuthorizationForKey')->willReturn(true);

        $this->viewGradeService
            ->method('getCollectionByTermCodesCrnsUniqueIdsTypes')
            ->willThrowException(new InvalidTermCodeException());

        $response = $this->getJson('/grade/v2/grade?termCode=EEEEEE&crn=70591&token=liaom');
        $response->assertStatus(App::API_BADREQUEST);
    }

    public function testInvalidCRN()
    {
        $this->user->method('getUsername')->willReturn('liaom');
        $this->user->method('checkAuthorization')->willReturn('admin');
        $this->authorizationValidator->method('validateAuthorizationForKey')->willReturn(true);

        $this->viewGradeService
            ->method('getCollectionByTermCodesCrnsUniqueIdsTypes')
            ->willThrowException(new InvalidCrnException());

        $response = $this->getJson('/grade/v2/grade?termCode=201720&crn=fffwfawffw&token=liaom');
        $response->assertStatus(App::API_BADREQUEST);
    }

    public function testInvalidGradeType()
    {
        $this->user->method('getUsername')->willReturn('liaom');
        $this->user->method('checkAuthorization')->willReturn('admin');
        $this->authorizationValidator->method('validateAuthorizationForKey')->willReturn(true);

        $this->viewGradeService
            ->method('getCollectionByTermCodesCrnsUniqueIdsTypes')
            ->willThrowException(new InvalidGradeTypeException());

        $response = $this->getJson('/grade/v2/grade?termCode=201720&crn=70591&type=fesfsefsef&token=liaom');
        $response->assertStatus(App::API_BADREQUEST);
    }

    public function testInvalidCourseSectionGuid()
    {
        $this->user->method('getUsername')->willReturn('liaom');
        $this->user->method('checkAuthorization')->willReturn('admin');
        $this->authorizationValidator->method('validateAuthorizationForKey')->willReturn(true);

        $this->viewGradeService
            ->method('getCollectionByCourseSectionGuidsUniqueIdsTypes')
            ->willThrowException(new InvalidCourseSectionGuidException());

        $response = $this->getJson('/grade/v2/grade?courseSectionGuid=ewfwfewfwefe&token=liaom');
        $response->assertStatus(App::API_BADREQUEST);
    }

    public function testGetCollectionByTermCrnUniqueIdTypeWithAdminTokenUser()
    {
        $this->user->method('getUsername')->willReturn('liaom');
        $this->user->method('checkAuthorization')->willReturn('admin');
        $this->authorizationValidator->method('validateAuthorizationForKey')->willReturn(true);

        $gradeMidTerm = $this->mockGrade('A','S','midterm',$this->guid,'70591','201720','liaom');
        $gradeFinal = $this->mockGrade('B','S','final',$this->guid,'70591','201720','liaom');
        $gradeCollection = new GradeCollection([$gradeMidTerm, $gradeFinal]);

        $this->viewGradeService
            ->method('getCollectionByTermCodesCrnsUniqueIdsTypes')
            ->with(['201720'],['70591'],['liaom'],['midterm','final'])
            ->willReturn($gradeCollection);

        $response = $this->getJson('/grade/v2/grade?termCode=201720&crn=70591&uniqueId=liaom&token=liaom');

        $response->assertStatus(App::API_OK);
        $response->assertJson([
            'data' => [
                [
                    'crn' => '70591',
                    'uniqueId' => 'liaom',
                    'courseSectionGuid' => $this->guid,
                    'termCode' => '201720',
                    'modeCode' => 'S',
                    'value' => 'A',
                    'type' => 'midterm'
                ],
                [
                    'crn' => '70591',
                    'uniqueId' => 'liaom',
                    'courseSectionGuid' => $this->guid,
                    'termCode' => '201720',
                    'modeCode' => 'S',
                    'value' => 'B',
                    'type' => 'final'
                ]
            ]
        ]);
    }

    public function testGetCollectionByTermCrnUniqueIdMidtermTypeWithAdminTokenUser()
    {
        $this->user->method('getUsername')->willReturn('liaom');
        $this->user->method('checkAuthorization')->willReturn('admin');
        $this->authorizationValidator->method('validateAuthorizationForKey')->willReturn(true);

        $gradeMidTerm = $this->mockGrade('A','S','midterm',$this->guid,'70591','201720','liaom');
             $gradeCollection = new GradeCollection([$gradeMidTerm]);

        $this->viewGradeService
            ->method('getCollectionByTermCodesCrnsUniqueIdsTypes')
            ->with(['201720'],['70591'],['liaom'],['midterm'])
            ->willReturn($gradeCollection);

        $response = $this->getJson('/grade/v2/grade?termCode=201720&crn=70591&uniqueId=liaom&type=midterm&token=liaom');

        $response->assertStatus(App::API_OK);
        $response->assertJson([
            'data' => [
                [
                    'crn' => '70591',
                    'uniqueId' => 'liaom',
                    'courseSectionGuid' => $this->guid,
                    'termCode' => '201720',
                    'modeCode' => 'S',
                    'value' => 'A',
                    'type' => 'midterm'
                ]
            ]
        ]);
    }

    public function testGetCollectionByTermCrnUniqueIdTypeWithoutUniqueIdWithAdminTokenUser()
    {
        $this->user->method('getUsername')->willReturn('liaom');
        $this->user->method('checkAuthorization')->willReturn('admin');
        $this->authorizationValidator->method('validateAuthorizationForKey')->willReturn(true);

        $gradeMidTerm = $this->mockGrade('A','S','midterm',$this->guid,'70591','201720','liaom');
        $gradeFinal = $this->mockGrade('B','S','final',$this->guid,'70591','201720','liaom');
        $gradeCollection = new GradeCollection([$gradeMidTerm, $gradeFinal]);

        $this->viewGradeService
            ->method('getCollectionByTermCodesCrnsUniqueIdsTypes')
            ->with(['201720'],['70591'],[],['midterm','final'])
            ->willReturn($gradeCollection);

        $response = $this->getJson('/grade/v2/grade?termCode=201720&crn=70591&token=liaom');

        $response->assertStatus(App::API_OK);
        $response->assertJson([
            'data' => [
                [
                    'crn' => '70591',
                    'uniqueId' => 'liaom',
                    'courseSectionGuid' => $this->guid,
                    'termCode' => '201720',
                    'modeCode' => 'S',
                    'value' => 'A',
                    'type' => 'midterm'
                ],
                [
                    'crn' => '70591',
                    'uniqueId' => 'liaom',
                    'courseSectionGuid' => $this->guid,
                    'termCode' => '201720',
                    'modeCode' => 'S',
                    'value' => 'B',
                    'type' => 'final'
                ]
            ]
        ]);
    }

    public function testGetCollectionByTermCrnUniqueIdTypeWithTokenUserNotAdmin()
    {
        $this->user->method('getUsername')->willReturn('liaom');
        $this->user->method('checkAuthorization')->willReturn(null);
        $this->authorizationValidator->method('validateAuthorizationForKey')->willReturn(false);

        $gradeMidTerm = $this->mockGrade('A','S','midterm',$this->guid,'70591','201720','liaom');
        $gradeFinal = $this->mockGrade('B','S','final',$this->guid,'70591','201720','liaom');
        $gradeCollection = new GradeCollection([$gradeMidTerm, $gradeFinal]);

        $this->viewGradeService
            ->method('getCollectionByTermCodesCrnsUniqueIdsTypes')
            ->with(['201720'],['70591'],['liaom'],['midterm'])
            ->willReturn($gradeCollection);

        $instructorAssignment = $this->mockInstructor($this->instructorGuid,'liaom','201720','70591',$this->guid,'true');
        $InstructorAssignmentCollection = new InstructorAssignmentCollection([$instructorAssignment]);

        $this->viewInstructorAssignmentService
            ->method('getCollectionByTermCodesCrnsUniqeIds')
            ->with(['201720'],['70591'],['liaom'])
            ->willReturn($InstructorAssignmentCollection);

        $response = $this->getJson('/grade/v2/grade?termCode=201720&crn=70591&uniqueId=liaom&type=midterm&token=liaom');
        $response->assertStatus(App::API_OK);
        $response->assertJson([
            'data' => [
                [
                    'crn' => '70591',
                    'uniqueId' => 'liaom',
                    'courseSectionGuid' => $this->guid,
                    'termCode' => '201720',
                    'modeCode' => 'S',
                    'value' => 'A',
                    'type' => 'midterm'
                ],
                [
                    'crn' => '70591',
                    'uniqueId' => 'liaom',
                    'courseSectionGuid' => $this->guid,
                    'termCode' => '201720',
                    'modeCode' => 'S',
                    'value' => 'B',
                    'type' => 'final'
                ]
            ]
        ]);
    }

    public function testGetCollectionByCourseSectionGuidWithStandardUser()
    {
        $this->user->method('getUsername')->willReturn('liaom');
        $this->user->method('checkAuthorization')->willReturn(null);
        $this->authorizationValidator->method('validateAuthorizationForKey')->willReturn(false);

        $gradeMidTerm = $this->mockGrade('A','S','midterm',$this->guid,'70591','201720','liaom');
        $gradeFinal = $this->mockGrade('B','S','final',$this->guid,'70591','201720','liaom');
        $gradeCollection = new GradeCollection([$gradeMidTerm, $gradeFinal]);

        $this->viewGradeService
            ->method('getCollectionByCourseSectionGuidsUniqueIdsTypes')
            ->with([$this->guid],['liaom'],['midterm', 'final'])
            ->willReturn($gradeCollection);

        $instructorAssignment = $this->mockInstructor($this->instructorGuid,'liaom','201720','70591',$this->guid,'true');
        $InstructorAssignmentCollection = new InstructorAssignmentCollection([$instructorAssignment]);
        $this->viewInstructorAssignmentService
            ->method('getCollectionByCourseSectionGuidsUniqueIds')
            ->with([$this->guid],['liaom'])
            ->willReturn($InstructorAssignmentCollection);

        $response = $this->getJson('/grade/v2/grade?courseSectionGuid=' . $this-> guid . '&uniqueId=liaom&token=liaom');
        $response->assertStatus(App::API_OK);
        $response->assertJson([
            'data' => [
                [
                    'crn' => '70591',
                    'uniqueId' => 'liaom',
                    'courseSectionGuid' => $this->guid,
                    'termCode' => '201720',
                    'modeCode' => 'S',
                    'value' => 'A',
                    'type' => 'midterm'
                ],
                [
                    'crn' => '70591',
                    'uniqueId' => 'liaom',
                    'courseSectionGuid' => $this->guid,
                    'termCode' => '201720',
                    'modeCode' => 'S',
                    'value' => 'B',
                    'type' => 'final'
                ]
            ]
        ]);
    }

    public function testgetCollectionByCourseSectionIdsUniqueIdTypeWithAdminTokenUser()
    {
        $this->user->method('getUsername')->willReturn('liaom');
        $this->user->method('checkAuthorization')->willReturn('admin');
        $this->authorizationValidator->method('validateAuthorizationForKey')->willReturn(true);

        $gradeMidTerm = $this->mockGrade('A','S','midterm',$this->guid,'70591','201720','liaom');
        $gradeFinal = $this->mockGrade('B','S','final',$this->guid,'70591','201720','liaom');
        $gradeCollection = new GradeCollection([$gradeMidTerm, $gradeFinal]);

        $this->viewGradeService
            ->method('getCollectionByCourseSectionGuidsUniqueIdsTypes')
            ->with([$this->guid],['liaom'],['midterm','final'])
            ->willReturn($gradeCollection);

        $response = $this->getJson('/grade/v2/grade?courseSectionGuid=' . $this-> guid . '&uniqueId=liaom&token=liaom');

        $response->assertStatus(App::API_OK);
        $response->assertJson([
            'data' => [
                [
                    'crn' => '70591',
                    'uniqueId' => 'liaom',
                    'courseSectionGuid' => $this->guid,
                    'termCode' => '201720',
                    'modeCode' => 'S',
                    'value' => 'A',
                    'type' => 'midterm'
                ],
                [
                    'crn' => '70591',
                    'uniqueId' => 'liaom',
                    'courseSectionGuid' => $this->guid,
                    'termCode' => '201720',
                    'modeCode' => 'S',
                    'value' => 'B',
                    'type' => 'final'
                ]
            ]
        ]);
    }
}
