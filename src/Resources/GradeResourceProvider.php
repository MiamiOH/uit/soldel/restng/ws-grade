<?php

namespace MiamiOH\GradeWebService\Resources;

use MiamiOH\RESTng\App;

class GradeResourceProvider extends \MiamiOH\RESTng\Util\ResourceProvider
{
    public function registerDefinitions(): void
    {
        $this->addDefinition(array(
            'name' => 'Grade.Exception',
            'type' => 'object',
            'properties' => array(
                'message' => array(
                    'type' => 'string',
                ),
                'line' => array(
                    'type' => 'integer',
                ),
                'file' => array(
                    'type' => 'string',
                )
            )
        ));

        $this->addDefinition([
            'name' => 'grade',
            'type' => 'object',
            'properties' => [
                'termCode' => [
                    'type' => 'string',
                ],
                'crn' => [
                    'type' => 'string',
                ],
                'uniqueId' => [
                    'type' => 'string',
                ],
                'courseSectionGuid' => [
                    'type' => 'string',
                ],
                'type' => [
                    'type' => 'string',
                ],
                'value' => [
                    'type' => 'string',
                ],
            ]
        ]);

        $this->addDefinition([
            'name' => 'Grade.Update.Request',
            'type' => 'object',
            'properties' => [
                'courseSectionGuid' => [
                    'type' => 'string',
                    'required' => true,
                    'example' => '954b6b70-a876-4a72-ac79-6b481fc22778'
                ],
                'uniqueId' => [
                    'type' => 'string',
                    'required' => true,
                    'example' => 'liaom'
                ],
                'gradeType' => [
                    'type' => 'string',
                    'required' => true,
                    'example' => 'midterm'
                ],
                'grade' => [
                    'type' => 'string',
                    'required' => true,
                    'example' => 'A'
                ],
                'hasAttended' => [
                    'type' => 'boolean',
                    'example' => true
                ],
                'lastAttendDate' => [
                    'type' => 'date',
                    'example' => '2018-03-31'
                ],
                'submittedBy' => [
                    'type' => 'string',
                    'example' => 'uniqueid1'
                ],
            ]
        ]);

        $this->addDefinition([
            'name' => 'Grade.Update.Request.Collection',
            'type' => 'array',
            'items' => [
                '$ref' => '#/definitions/Grade.Update.Request'
            ]
        ]);


        $this->addDefinition([
            'name' => 'Grade.Collection',
            'type' => 'array',
            'items' => [
                '$ref' => '#/definitions/grade'
            ]
        ]);

        $this->addDefinition([
            'name' => 'Grade.Update.Response',
            'type' => 'object',
            'properties' => [
                'isSuccessful' => [
                    'type' => 'boolean',
                    'example' => true
                ],
                'message' => [
                    'type' => 'string',
                    'example' => 'Ok'
                ],
            ]
        ]);

        $this->addDefinition([
            'name' => 'Grade.Update.Response.Collection',
            'type' => 'array',
            'items' => [
                '$ref' => '#/definitions/Grade.Update.Response'
            ]
        ]);
    }

    public function registerServices(): void
    {
        $this->addService([
            'name' => 'Grade.grade',
            'class' => 'MiamiOH\GradeWebService\Services\GradeService',
            'description' => 'Grades',
            'set' => [
                'pikeServiceFactory' => [
                    'type' => 'service',
                    'name' => 'PikeServiceFactory'
                ],
            ],

        ]);
    }

    public function registerResources(): void
    {
        $this->addResource([
            'action' => 'read',
            'name' => 'grade.v2.grades',
            'description' => "##### Description:\nThis resource provides get grades function to the consumer application. One grade is returned either by term code plus 
                      crn or by courseSectionGuid.
                      \n##### Authentication:\nConsumer must provide a valid Miami WS token
                      \n##### Authorization:
                      \n- With admin access, consumers are able to get any grade. Contact Solution Delivery Support to add your UniqueID with key `admin` into AuthMan (application: `WebServices`, module: `GradeService`).
                      \n- Without admin access, instructor can ONLY get grades of students they taught and student can ONLY get his/her own grades.
                      ",
            'summary' => 'get a collection of grades.',
            'pattern' => '/grade/v2/grade',
            'service' => 'Grade.grade',
            'tags' => ['grade'],
            'method' => 'getGradeCollection',
            'returnType' => 'collection',
            'options' => [
                'termCode' => [
                    'description' => 'Term code',
                    'type' => 'list'
                ],
                'crn' => [
                    'description' => 'Course crn',
                    'type' => 'list'
                ],
                'uniqueId' => [
                    'description' => 'Student\'s UniqueID that you want to get grade for. It must 
                              be a valid Miami UniqueID',
                    'type' => 'list'
                ],
                'courseSectionGuid' => [
                    'description' => 'A valid course section GUID from SATURN.SSBSGID table.',
                    'type' => 'list'
                ],
                'type' => ['description' => 'Either \'midterm\' or \'final\'. Default is midterm and final.',
                    'type' => 'list'
                ],
            ],
            'middleware'    => [
                'authenticate' => ['type' => 'token'],
            ],
            'responses' => [
                App::API_OK => [
                    'description' => 'A collection of grades',
                    'returns' => [
                        'type' => 'array',
                        '$ref' => '#/definitions/Grade.Collection',
                    ]
                ],
                App::API_BADREQUEST =>[
                    'description' => 'Server could not understand the request due to invalid syntax',
                    'returns' => [
                        'type' => 'object',
                        '$ref' => '#/definitions/Grade.Exception',
                    ],
                ],
                App::API_UNAUTHORIZED =>[
                    'description' => 'The user could not be authenticated or is not authorized to access the resource.',
                    'returns' => [
                        'type' => 'object',
                        '$ref' => '#/definitions/Grade.Exception',
                    ],
                ],
                App::API_FAILED =>[
                    'description' => 'Server Error',
                    'returns' => [
                        'type' => 'object',
                        '$ref' => '#/definitions/Grade.Exception',
                    ],
                ],
            ]
        ]);

        $this->addResource([
            'action' => 'update',
            'name' => 'grade.v2.grades.update',
            'description' => "##### Description:\nThis resource provides bulk update grades function to the consumer application. Each\nrequest will be proceed individually. Errors happened during update process will not\nstop the whole process. Instead, they will be logged into response body and return to \nconsumer after process end.\n\n##### Authentication:\nConsumer must provide a valid Miami WS token\n\n##### Authorization:\n- With admin access, consumers are able to update any grade. Contact Solution Delivery\nSupport to add your UniqueID with key `admin` into AuthMan (application: `WebServices`\n, module: `GradeService`).\n- Consumers are ONLY able to update grades of students they taught.\n\n##### Request Body:\nA collection of requests which contains following attributes:\n- courseSectionGuid (required): a valid course section GUID from SATURN.SSBSGID table.\n- uniqueId (required): student's UniqueID that you want to update grade for. It must \nbe a valid Miami UniqueID\n- gradeType (required): either 'midterm' or 'final'\n- grade (required): a valid grade value. e.g. A, B+, etc.\n- hasAttended (optional): either true or false\n- lastAttendDate (optional): a valid date in YYYY-MM-DD format. e.g. 2018-03-31\n\n##### Response Body:\nA collection of update status which matches the order of requests in the request body\nand contains following attributes:\n- isSuccessful: either true or false\n- message: message is always \"Ok\" if grade update successfully. Otherwise, message \ncan be vary which depend on error happened during update process",
            'summary' => 'Bulk update grades',
            'pattern' => '/grade/v2/grade',
            'service' => 'Grade.grade',
            'tags' => ['grade'],
            'method' => 'putGrade',
            'middleware' => [
                'authenticate' => ['type' => 'token'],
            ],
            'body' => [
                'description' => 'Collection of grades to be updated',
                'required' => true,
                'schema' => [
                    '$ref' => '#/definitions/Grade.Update.Request.Collection'
                ]
            ],
            'responses' => [
                App::API_OK => [
                    'description' => 'A collection of grades',
                    'returns' => [
                        'type' => 'array',
                        '$ref' => '#/definitions/Grade.Update.Response.Collection',
                    ]
                ],
                App::API_BADREQUEST =>[
                    'description' => 'Server could not understand the request due to invalid syntax',
                    'returns' => [
                        'type' => 'object',
                        '$ref' => '#/definitions/Grade.Exception',
                    ],
                ],
                App::API_UNAUTHORIZED =>[
                    'description' => 'The user could not be authenticated or is not authorized to access the resource.',
                    'returns' => [
                        'type' => 'object',
                        '$ref' => '#/definitions/Grade.Exception',
                    ],
                ],
                App::API_FAILED =>[
                    'description' => 'Server Error',
                    'returns' => [
                        'type' => 'object',
                        '$ref' => '#/definitions/Grade.Exception',
                    ],
                ],
            ]
        ]);
    }

    public function registerOrmConnections(): void
    {
    }
}
