<?php
namespace MiamiOH\GradeWebService\Services;

use MiamiOH\RESTng\Exception\BadRequest;

class GradeModeService extends BannerBase
{
    public function queryGradeModes()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();
        $dbh = $this->getDatabaseHandle();

        $level = $request->getResourceParam('gradeLevel');
        $mode = $request->getResourceParam('gradeMode');

        $vars = array();

        $sql =
'SELECT DISTINCT szvgrde_gmod_code, szvgrde_gmod_desc, szvgrde_level_code FROM BANINST1.szvgrde';

        if ($level) {
            $sql .= ' WHERE szvgrde_level_code=:GRD_LEVEL';
            $vars['GRD_LEVEL'] = $level;
            if ($mode) {
                $sql .= ' AND szvgrde_gmod_code=:GRD_MODE';
                $vars['GRD_MODE'] = $mode;
            }
        }

        $data = array();
        $sth = $this->bindquery($sql, $vars);
        while ($row = $sth->fetchrow_assoc()) {
            $newrow['gradeModeId'] = $row['szvgrde_gmod_code'];
            $newrow['gradeModeName'] = $row['szvgrde_gmod_desc'];
            $newrow['gradeLevel'] = $row['szvgrde_level_code'];

            $data[] = $newrow;
        }
        
        return $data;
    }

    public function getGradeModes()
    {
        return $this->makeCollectionResponse($this->queryGradeModes());
    }

    public function getGradeModeID()
    {
        return $this->makeSingleResponse($this->queryGradeModes()[0]);
    }

    public function queryGradeModeValues()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();
        $dbh = $this->getDatabaseHandle();

        $level = $request->getResourceParam('gradeLevel');
        $mode = $request->getResourceParam('gradeModeId');
        $type = $request->getResourceParam('gradeType');
        $value = $request->getResourceParam('gradeValue');

        if ($type && $type!='final' && $type!='midterm') {
            throw new BadRequest('An invalid grade type was given');
        }

        $sql =
'SELECT DISTINCT szvgrde_gmod_code, szvgrde_gmod_desc, szvgrde_level_code, 
szvgrde_grde_code, szvgrde_grde_desc, szvgrde_passed_ind
FROM BANINST1.szvgrde';

        $vars = array();

        if ($level) {
            $sql .= ' WHERE szvgrde_level_code=:GRD_LEVEL';
            $vars['GRD_LEVEL'] = $level;
            if ($mode) {
                $sql .= ' AND szvgrde_gmod_code=:GRD_MODE';
                $vars['GRD_MODE'] = $mode;
                if ($value) {
                    $sql .= ' AND szvgrde_grde_code=:GRD_VALUE';
                    $vars['GRD_VALUE'] = $value;
                }
            }
        }


        $data = array();
        $sth = $this->bindquery($sql, $vars);
        while ($row = $sth->fetchrow_assoc()) {
            $newrow['gradeModeId'] = $row['szvgrde_gmod_code'];
            $newrow['gradeModeName'] = $row['szvgrde_gmod_desc'];
            $newrow['gradeLevel'] = $row['szvgrde_level_code'];
            $newrow['gradeValue'] = $row['szvgrde_grde_code'];
            $newrow['gradeName'] = $row['szvgrde_grde_desc'];
            $newrow['passingGrade'] = $row['szvgrde_passed_ind'];

            if ($newrow['passingGrade'] == "Y") {
                $newrow['attendanceInformation']  = "Not Allowed";
            } elseif ($newrow['passingGrade'] == "N") {
                $newrow['attendanceInformation'] = "Required";
            }

            if ($type != 'final') {
                $newrow['gradeType'] = 'midterm';
                $data[] = $newrow;
            }
            if ($type != 'midterm') {
                $newrow['gradeType'] = 'final';
                $data[] = $newrow;
            }
        }
        return $data;
    }

    public function getGradeModeValues()
    {
        return $this->makeCollectionResponse($this->queryGradeModeValues());
    }

    public function getGradeModeValueID()
    {
        return $this->makeSingleResponse($this->queryGradeModeValues()[0]);
    }

    public function queryGradeModeTranslations()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();
        $options = $request->getOptions();
        $dbh = $this->getDatabaseHandle();

        $level = $request->getResourceParam('gradeLevel');
        $mode = $request->getResourceParam('gradeMode');
        $extmode = $request->getResourceParam('externalGradeMode');
        $extgrade = $request->getResourceParam('externalGrade');

        $sql =
'SELECT DISTINCT szvgmvt_levl, szvgmvt_nonban_grd_mode, szvgmvt_nonban_grd_mode_desc, 
szvgmvt_nonban_grd, szvgmvt_ban_grd_mode, szvgmvt_ban_grd_mode_desc, szvgmvt_ban_grd
FROM saturn.szvgmvt';

        $vars = array();
        $data = array();

        if ($level) {
            $sql .= ' WHERE szvgmvt_levl=:GRD_LEVEL';
            $vars['GRD_LEVEL'] = $level;
            if ($mode) {
                $sql .= ' AND szvgmvt_ban_grd_mode=:GRD_MODE';
                $vars['GRD_MODE'] = $mode;
                if ($extmode) {
                    $sql .= ' AND szvgmvt_nonban_grd_mode=:EXT_MODE';
                    $vars['EXT_MODE'] = $extmode;
                    if ($extgrade) {
                        $sql .= ' AND szvgmvt_nonban_grd=:EXT_GRD';
                        $vars['EXT_GRD'] = $extgrade;
                    }
                }
            }
        }

        $sth = $this->bindquery($sql, $vars);
        while ($row = $sth->fetchrow_assoc()) {
            $newrow['gradeLevel'] = $row['szvgmvt_levl'];
            $newrow['gradeModeId'] = $row['szvgmvt_ban_grd_mode'];
            $newrow['gradeModeName'] = $row['szvgmvt_ban_grd_mode_desc'];
            $newrow['externalGradeMode'] = $row['szvgmvt_nonban_grd_mode'];
            $newrow['externalGradeModeName'] = $row['szvgmvt_nonban_grd_mode_desc'];
            $newrow['externalGrade'] = $row['szvgmvt_nonban_grd'];
            $newrow['gradeValue'] = $row['szvgmvt_ban_grd'];
            $data[] = $newrow;
        }
        return $data;
    }

    public function getGradeModeTranslations()
    {
        return $this->makeCollectionResponse($this->queryGradeModeTranslations());
    }

    public function getGradeModeTranslationID()
    {
        return $this->makeSingleResponse($this->queryGradeModeTranslations()[0]);
    }
}
