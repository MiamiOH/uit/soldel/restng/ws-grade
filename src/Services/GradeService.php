<?php

namespace MiamiOH\GradeWebService\Services;

use Carbon\Carbon;
use MiamiOH\Pike\App\Framework\RESTng\PikeServiceFactory;
use MiamiOH\Pike\App\Mapper\AppMapper;
use MiamiOH\Pike\App\Service\UpdateGradeService;
use MiamiOH\Pike\App\Service\ViewGradeService;
use MiamiOH\Pike\App\Service\ViewInstructorAssignmentService;
use MiamiOH\Pike\Domain\Collection\GradeCollection;
use MiamiOH\Pike\Domain\Collection\UpdateGradeRequestCollection;
use MiamiOH\Pike\Domain\Model\Grade;
use MiamiOH\Pike\Domain\Model\InstructorAssignment;
use MiamiOH\Pike\Domain\Request\UpdateGradeRequest;
use MiamiOH\Pike\Domain\ValueObject\UpdateGradeResponse;
use MiamiOH\Pike\Exception\InvalidArgumentException;
use MiamiOH\Pike\Exception\NotFoundException;
use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Exception\BadRequest;
use MiamiOH\RESTng\Service;
use MiamiOH\RESTng\Util\Request;
use MiamiOH\RESTng\Util\Response;

/**
 * Created by PhpStorm.
 * User: xiaw
 * Date: 5/17/18
 * Time: 10:46 AM
 */
class GradeService extends Service
{
    /**
     * @var string
     */
    private $datasource_name = 'MUWS_SEC_PROD';

    /**
     * @var Request
     */
    private $request;

    /**
     * @var array
     */
    private $options;

    /**
     * @var Response
     */
    private $response;

    /**
     * @var AppMapper
     */
    private $pike;

    /**
     * @var ViewGradeService
     */
    private $viewGradeService;
    /**
     * @var UpdateGradeService
     */
    private $updateGradeService;

    /**
     * @var ViewInstructorAssignmentService
     */
    private $viewInstructorAssignmentService;

    /**
     * @var boolean|null
     */
    private $isAdmin = null;

    private function setup()
    {
        $this->request = $this->getRequest();
        $this->response = $this->getResponse();
        $this->options = $this->request->getOptions();

        if (!isset($this->options['type'])) {
            $this->options['type'] = ['midterm', 'final'];
        }
    }

    public function setPikeServiceFactory(PikeServiceFactory $pikeServiceFactory)
    {
        $this->pike = $pikeServiceFactory->getEloquentPikeServiceMapper($this->datasource_name);
        $this->viewGradeService = $this->pike->getViewGradeService();
        $this->updateGradeService = $this->pike->getUpdateGradeService();
        $this->viewInstructorAssignmentService = $this->pike->getViewInstructorAssignmentService();
    }

    public function getGradeCollectionByTermCodesCrnsUniqueIdsTypes(
        array $termCodes,
        array $crns,
        array $uniqueIds,
        array $types
    ) {
        $gradeCollection = null;

        $gradeCollection = $this->viewGradeService->getCollectionByTermCodesCrnsUniqueIdsTypes(
            $termCodes,
            $crns,
            $uniqueIds,
            $types
        );

        return $gradeCollection;
    }

    public function getCollectionByCourseSectionIdsUniqueIdsTypes(
        array $courseSectionGuid,
        array $uniqueIds,
        array $types
    ) {
        $gradeCollection = null;
        $gradeCollection = $this->viewGradeService->getCollectionByCourseSectionGuidsUniqueIdsTypes(
            $courseSectionGuid,
            $uniqueIds,
            $types
        );

        return $gradeCollection;
    }


    public function getGradeCollection()
    {
        $this->setup();

        $this->validateQueryParameters();

        $gradeCollection = null;

        try {
            if (!isset($this->options['courseSectionGuid'])) {
                $gradeCollection = $this->getGradeCollectionByTermCodesCrnsUniqueIdsTypes(
                    $this->getQueryParameterValueByKey('termCode'),
                    $this->getQueryParameterValueByKey('crn'),
                    $this->getQueryParameterValueByKey('uniqueId'),
                    $this->getQueryParameterValueByKey('type')
                );
            } else {
                $gradeCollection = $this->getCollectionByCourseSectionIdsUniqueIdsTypes(
                    $this->getQueryParameterValueByKey('courseSectionGuid'),
                    $this->getQueryParameterValueByKey('uniqueId'),
                    $this->getQueryParameterValueByKey('type')
                );
            }
        } catch (NotFoundException $e) {
            $this->response->setStatus(App::API_NOTFOUND);

            return $this->response;
        } catch (InvalidArgumentException $e) {
            $this->response->setStatus(App::API_BADREQUEST);
            $this->response->setPayload([$e->getMessage()]);

            return $this->response;
        }
        $isAdmin = $this->isAdmin();


        if ($isAdmin) {
            $this->addGradeCollectionToPayload($gradeCollection);
        } else {
            $tokenUser = $this->getApiUser()->getUsername();
            $tokenUserInstructorAssignmentCollection = null;

            if (!isset($this->options['courseSectionGuid'])) {
                $tokenUserInstructorAssignmentCollection = $this->pike
                    ->getViewInstructorAssignmentService()
                    ->getCollectionByTermCodesCrnsUniqeIds(
                        $this->getQueryParameterValueByKey('termCode'),
                        $this->getQueryParameterValueByKey('crn'),
                        array($tokenUser)
                    );
            } else {
                $tokenUserInstructorAssignmentCollection = $this->pike
                    ->getViewInstructorAssignmentService()
                    ->getCollectionByCourseSectionGuidsUniqueIds(
                        $this->options['courseSectionGuid'],
                        [$tokenUser]
                    );
            }


            $courseSectionGuidsTokenUserTeach = $tokenUserInstructorAssignmentCollection->map(
                function (InstructorAssignment $instructorAssignment) {
                    return $instructorAssignment->getCourseSectionGuid()->getValue();
                }
            )->toArray();

            $filtered = $gradeCollection->filter(
                function (Grade $grade) use (
                    $tokenUser,
                    $courseSectionGuidsTokenUserTeach
                ) {
                    return ($grade->getUniqueId()->getUppercaseValue() === strtoupper($tokenUser)) || in_array(
                        $grade->getCourseSectionGuid()->getValue(),
                        $courseSectionGuidsTokenUserTeach
                    );
                }
            );

            $this->addGradeCollectionToPayload($filtered);
        }


        return $this->response;
    }

    public function isAdmin()
    {
        if ($this->isAdmin === null) {
            $this->isAdmin = $this->getApiUser()
                ->checkAuthorization(
                    $this->buildAuthorizationConfigByKey('admin')
                );
        }

        return $this->isAdmin;
    }

    protected function buildAuthorizationConfigByKey(string $key): array
    {
        return [
            'type' => 'authMan',
            'application' => 'WebServices',
            'module' => 'GradeService',
            'key' => [$key],
        ];
    }


    public function getQueryParameterValueByKey(
        string $key,
        bool $castToLowerCase = true
    ): ?array {
        if (isset($this->options[$key])) {
            $value = $this->options[$key];
            if ($castToLowerCase) {
                $value = array_map(function ($value) {
                    return strtolower(trim($value));
                }, $value);
            }

            return $value;
        }

        return [];
    }

    private function validateQueryParameters()
    {
        if (!isset($this->options['crn']) && !isset($this->options['uniqueId']) && !isset($this->options['courseSectionGuid'])) {
            throw new BadRequest('query parameter, must enter either "UniquieId" or "crn".');
        }

        return [];
    }

    private function gradeToArray(Grade $grade): array
    {
        return [
            'crn' => (string)$grade->getCrn(),
            'uniqueId' => (string)$grade->getUniqueId(),
            'courseSectionGuid' => (string)$grade->getCourseSectionGuid(),
            'termCode' => (string)$grade->getTermCode(),
            'modeCode' => (string)$grade->getGradeModeCode(),
            'value' => (string)$grade->getValue(),
            'type' => (string)$grade->getType(),

        ];
    }

    private function addToPayload(array $data)
    {
        $payload = $this->response->getPayload();

        $payload = array_merge($payload, $data);

        $this->response->setPayload($payload);
    }


    private function gradeCollectionToArray(GradeCollection $gradeCollection): array
    {
        $data = [];

        foreach ($gradeCollection as $grade) {
            $data[] = $this->gradeToArray($grade);
        }

        return $data;
    }

    private function addGradeCollectionToPayload(GradeCollection $gradeCollection)
    {
        $this->addToPayload($this->gradeCollectionToArray($gradeCollection));
    }


    public function putGrade()
    {
        $response = $this->getResponse();
        $request = $this->getRequest();
        $requestBody = $request->getData();
        $tokenUser = $this->getApiUser()->getUsername();
        $responseBody = [];
        $updateGradeRequestCollection = new UpdateGradeRequestCollection();

        foreach ($requestBody as $index => $data) {
            $submittedBy = strtolower($data['submittedBy'] ?? null);
            if (trim($submittedBy) === '') {
                $submittedBy = null;
            }

            if (!$this->isAdmin() && $submittedBy && $submittedBy !== $tokenUser) {
                $responseBody[$index] = $this->constructUpdateGradeResponse(
                    false,
                    sprintf('You (%s) are not authorized to impersonate user (%s). Impersonation required admin privilege.', $tokenUser, $submittedBy)
                );
                continue;
            }

            try {
                $updateGradeRequestCollection[$index] = $this->constructUpdateGradeRequest(
                    $data,
                    $submittedBy ?? $tokenUser
                );
            } catch (\Exception $e) {
                $responseBody[$index] = $this->constructUpdateGradeResponse(
                    false,
                    $e->getMessage()
                );
            }
        }

        if (!$this->isAdmin()) {
            $courseSectionGuids = $updateGradeRequestCollection->map(
                function (UpdateGradeRequest $request) {
                    return $request->getCourseSectionGuid()->getValue();
                }
            )->toArray();
            $assignments = $this->viewInstructorAssignmentService
                ->getCollectionByCourseSectionGuidsUniqueIds(
                    $courseSectionGuids,
                    [$tokenUser]
                );
            $assignmentLookup = [];
            /** @var InstructorAssignment $assignment */
            foreach ($assignments as $assignment) {
                $assignmentLookup[$assignment->getCourseSectionGuid()->getValue()] = true;
            }
            /** @var UpdateGradeRequest $request */
            foreach ($updateGradeRequestCollection as $index => $request) {
                if (!isset($assignmentLookup[$request->getCourseSectionGuid()->getValue()])) {
                    $responseBody[$index] = $this->constructUpdateGradeResponse(
                        false,
                        'You are unauthorized to update students\' grades in this course section.'
                    );
                    unset($updateGradeRequestCollection[$index]);
                }
            }
        }

        $updateGradeResponseCollection = $this->updateGradeService->bulkUpdate($updateGradeRequestCollection);
        /** @var UpdateGradeResponse $updateGradeResponse */
        foreach ($updateGradeResponseCollection as $index => $updateGradeResponse) {
            $responseBody[$index] = $this->constructUpdateGradeResponse(
                $updateGradeResponse->isSuccessful(),
                $updateGradeResponse->getMessage()
            );
        }

        $rearrangedResponseBody = [];
        for ($i = 0; $i < count($requestBody); $i++) {
            $rearrangedResponseBody[] = $responseBody[$i];
        }

        $response->setPayload($rearrangedResponseBody);
        $response->setStatus(App::API_OK);

        return $response;
    }

    private function constructUpdateGradeResponse(
        bool $isSuccessful,
        string $message
    ): array {
        return [
            'isSuccessful' => $isSuccessful,
            'message' => $message,
        ];
    }

    private function constructUpdateGradeRequest(
        array $data,
        string $tokenUser
    ): UpdateGradeRequest {
        $courseSectionGuid = $data['courseSectionGuid'] ?? null;
        $uniqueId = $data['uniqueId'] ?? null;
        $gradeType = $data['gradeType'] ?? null;
        $grade = $data['grade'] ?? null;
        $hasAttended = $data['hasAttended'] ?? null;
        $lastAttendDate = $data['lastAttendDate'] ?? null;

        if ($courseSectionGuid === null) {
            throw new \InvalidArgumentException('Missing courseSectionGuid');
        }

        if ($uniqueId === null) {
            throw new \InvalidArgumentException('Missing uniqueId');
        }

        if ($gradeType === null) {
            throw new \InvalidArgumentException('Missing gradeType');
        }

        if ($grade === null) {
            throw new \InvalidArgumentException('Missing grade');
        }

        if ($hasAttended !== null && $hasAttended !== true && $hasAttended !== false) {
            throw new \InvalidArgumentException('Invalid hasAttended, must be either true or false');
        }

        $lastAttendDate = $this->stringToDate($lastAttendDate);
        if ($lastAttendDate !== null) {
            $lastAttendDate->setTime(0, 0, 0, 0);
        }

        return new UpdateGradeRequest(
            $courseSectionGuid,
            $uniqueId,
            $gradeType,
            $grade,
            $tokenUser,
            $hasAttended,
            $lastAttendDate
        );
    }

    private function stringToDate(?string $dateString): ?Carbon
    {
        if ($dateString === null) {
            return null;
        }

        try {
            return Carbon::createFromFormat('Y-m-d', $dateString);
        } catch (\Exception $e) {
            throw new \InvalidArgumentException('Invalid lastAttendDate');
        }
    }
}
