<?php

namespace MiamiOH\GradeWebService\Tests\Unit;

use MiamiOH\GradeWebService\Services\GradeService;
use MiamiOH\Pike\App\Framework\RESTng\PikeServiceFactory;
use MiamiOH\Pike\App\Mapper\AppMapper;
use MiamiOH\Pike\App\Service\ViewInstructorAssignmentService;
use MiamiOH\Pike\App\Service\ViewGradeService;
use MiamiOH\Pike\App\Service\UpdateGradeService;
use MiamiOH\Pike\Domain\Collection\GradeCollection;
use MiamiOH\Pike\Domain\Collection\InstructorAssignmentCollection;
use MiamiOH\Pike\Domain\Collection\UpdateGradeRequestCollection;
use MiamiOH\Pike\Domain\Collection\UpdateGradeResponseCollection;
use MiamiOH\Pike\Domain\Model\Grade;
use MiamiOH\Pike\Domain\Model\InstructorAssignment;
use MiamiOH\Pike\Domain\ValueObject\CourseSectionGuid;
use MiamiOH\Pike\Domain\ValueObject\Crn;
use MiamiOH\Pike\Domain\ValueObject\GradeModeCode;
use MiamiOH\Pike\Domain\ValueObject\GradeType;
use MiamiOH\Pike\Domain\ValueObject\GradeValue;
use MiamiOH\Pike\Domain\ValueObject\InstructorAssignmentGuid;
use MiamiOH\Pike\Domain\ValueObject\TermCode;
use MiamiOH\Pike\Domain\ValueObject\UniqueId;
use MiamiOH\Pike\Domain\ValueObject\UpdateGradeResponse;
use MiamiOH\RESTng\Testing\TestCase;
use MiamiOH\RESTng\Util\Request;
use MiamiOH\RESTng\Util\User;
use PHPUnit\Framework\MockObject\MockObject;
use ReflectionClass;

class GradeServiceTest extends TestCase
{
    /**
     * @var MockObject
     */
    protected $api;

    /**
     * @var MockObject
     */
    protected $request;

    /**
     * @var MockObject
     */
    protected $viewGradeService;

    /**
     * @var MockObject
     */
    protected $updateGradeService;

    /**
     * @var MockObject
     */
    protected $viewInstructorAssignmentService;

    /**
     * @var MockObject
     */
    protected $pikeServiceFactory;

    /**
     * @var MockObject
     */
    protected $pike;

    /**
     * @var GradeService
     */
    protected $gradeService;

    /**
     * @var Grade
     */
    protected $grade1;

    /**
     * @var Grade
     */
    protected $grade2;

    /**
     * @var array
     */
    private $grade1Payload;

    /**
     * @var array
     */
    private $grade2Payload;

    /**
     * @var array
     */
    private $gradePut1Payload;

    /**
     * @var array
     */
    private $gradePut2Payload;

    /**
     * @var InstructorAssignment
     */
    private $instructorAssignment1;
    /**
     * @var array
     */
    private $gradeData;
    /**
     * @var array
     */
    private $instructorAssignmentData;

    /**
     * @var User
     */
    private $apiUser;


    protected function setUp()
    {
        parent::setUp();

        $this->request = $this->createMock(Request::class);
        $this->gradeService = new GradeService();

        $this->viewGradeService = $this->createMock(ViewGradeService::class);
        $this->viewInstructorAssignmentService = $this->createMock(ViewInstructorAssignmentService::class);
        $this->updateGradeService = $this->createMock(UpdateGradeService::class);


        $this->pike = $this->createMock(AppMapper::class);
        $this->pike->method('getViewGradeService')->willReturn($this->viewGradeService);
        $this->pike->method('getViewInstructorAssignmentService')->willReturn($this->viewInstructorAssignmentService);
        $this->pike->method('getUpdateGradeService')->willReturn($this->updateGradeService);

        $this->pikeServiceFactory = $this->createMock(PikeServiceFactory::class);
        $this->pikeServiceFactory->method('getEloquentPikeServiceMapper')->willReturn($this->pike);

        $this->gradeService->setPikeServiceFactory($this->pikeServiceFactory);
        $this->gradeService->setRequest($this->request);

        $this->apiUser = $this->createMock(User::class);
        $this->gradeService->setApiUser($this->apiUser);

        $this->gradeData = array(
            "gradeValue" => "A",
            "gradeModeCode" => "B",
            "gradeType" => "midterm",
            "crn" => "432432",
            "termCode" => "201720",
            "uniqueId" => "liaom",
            "courseSectionGuid" => CourseSectionGuid::create(),
            "isPrimary" => true
        );

        $this->gradeData2 = array(
            "gradeValue" => "B",
            "gradeModeCode" => "B",
            "gradeType" => "final",
            "crn" => "432432",
            "termCode" => "201720",
            "uniqueId" => "liaom",
            "courseSectionGuid" => CourseSectionGuid::create(),
            "isPrimary" => true
        );

        $this->instructorAssignmentData = array(
            "instructorAssignmentGuid" => InstructorAssignmentGuid::create(),
            "crn" => "432432",
            "termCode" => "201720",
            "uniqueId" => "liaom",
            true
        );
        $this->grade1 = new Grade(
            new GradeValue($this->gradeData["gradeValue"]),
            new GradeModeCode($this->gradeData["gradeModeCode"]),
            new GradeType($this->gradeData["gradeType"]),
            $this->gradeData["courseSectionGuid"],
            new Crn($this->gradeData["crn"]),
            new TermCode($this->gradeData["termCode"]),
            new UniqueId($this->gradeData["uniqueId"])
        );

        $this->grade2 = new Grade(
            new GradeValue($this->gradeData2["gradeValue"]),
            new GradeModeCode($this->gradeData2["gradeModeCode"]),
            new GradeType($this->gradeData2["gradeType"]),
            $this->gradeData2["courseSectionGuid"],
            new Crn($this->gradeData2["crn"]),
            new TermCode($this->gradeData2["termCode"]),
            new UniqueId($this->gradeData2["uniqueId"])
        );

        $this->gradePut1 = new UpdateGradeResponse(
            0,
            true,
            'Is Successful'
        );

        $this->gradePut2 = new UpdateGradeResponse(
            0,
            false,
            'You are unauthorized to update students\' grades in this course section'
        );
        //\MiamiOH\Pike\Domain\ValueObject\UpdateGradeResponse::

        $this->instructorAssignment1 = new InstructorAssignment(
            $this->instructorAssignmentData["instructorAssignmentGuid"],
            new UniqueId($this->gradeData["uniqueId"]),
            new TermCode($this->gradeData["termCode"]),
            new Crn($this->gradeData["crn"]),
            $this->gradeData["courseSectionGuid"],
            $this->gradeData["isPrimary"]
        );

        $this->grade1Payload = [
            'crn' => $this->gradeData["crn"],
            'uniqueId' => $this->gradeData["uniqueId"],
            'courseSectionGuid' => (string)$this->gradeData["courseSectionGuid"],
            'termCode' => $this->gradeData["termCode"],
            'modeCode' => $this->gradeData["gradeModeCode"],
            'value' => $this->gradeData["gradeValue"],
            'type' => $this->gradeData["gradeType"]
        ];

        $this->grade2Payload = [
            'crn' => $this->gradeData2["crn"],
            'uniqueId' => $this->gradeData2["uniqueId"],
            'courseSectionGuid' => (string)$this->gradeData2["courseSectionGuid"],
            'termCode' => $this->gradeData2["termCode"],
            'modeCode' => $this->gradeData2["gradeModeCode"],
            'value' => $this->gradeData2["gradeValue"],
            'type' => $this->gradeData2["gradeType"]
        ];

        $this->gradePut1PayLoad = [
            [
                'isSuccessful' => true,
                'message' => 'Is Successful'
            ]
        ];

        $this->gradePut2PayLoad = [
            [
                'isSuccessful' => false,
                'message' => 'You are unauthorized to update students\' grades in this course section'
            ]
        ];
    }

    public function testGetGradeTypeDefaultToMidtermAndFinal()
    {
        $this->request->method('getOptions')->willReturn([
            'courseSectionGuid' => [(string)$this->gradeData["courseSectionGuid"]],
            'uniqueId' => ['liaom'],
            'type' => []

        ]);

        $this->apiUser->method('getUsername')->willReturn('liaom');
        $this->apiUser->method('checkAuthorization')->willReturn('admin');

        $this->viewGradeService
            ->method('getCollectionByCourseSectionGuidsUniqueIdsTypes')
            ->willReturn(new GradeCollection([$this->grade1, $this->grade2]));

        $actualResult = $this->gradeService->getGradeCollection()->getPayload();

        $this->assertEquals(
            [
                $this->grade1Payload,
                $this->grade2Payload
            ],
            $actualResult
        );
    }

    public function testGetGradeCollectionByCourseSectionGuidsWithAdminUser()
    {
        $this->request->method('getOptions')->willReturn([
            'courseSectionGuid' => [(string)$this->gradeData["courseSectionGuid"]],
            'uniqueId' => ['liaom'],
            'type' => ['midterm']

        ]);

        $this->apiUser->method('getUsername')->willReturn('liaom');
        $this->apiUser->method('checkAuthorization')->willReturn('admin');
        $this->viewGradeService
            ->method('getCollectionByCourseSectionGuidsUniqueIdsTypes')
            ->willReturn(new GradeCollection([$this->grade1]));

        $actualResult = $this->gradeService->getGradeCollection()->getPayload();

        $this->assertEquals(
            [
                $this->grade1Payload
            ],
            $actualResult
        );
    }

    public function testGetGradeCollectionByTermCodesAndCrnsWithAdminUser()
    {
        $this->request->method('getOptions')->willReturn([
            'crn' => ['12345678'],
            'termCode' => ['201720'],
            'uniqueId' => ['liaom'],
            'type' => ['midterm']

        ]);

        $this->apiUser->method('getUsername')->willReturn('liaom');
        $this->apiUser->method('checkAuthorization')->willReturn('admin');

        $this->viewGradeService
            ->method('getCollectionByTermCodesCrnsUniqueIdsTypes')
            ->willReturn(new GradeCollection([$this->grade1]));

        $actualResult = $this->gradeService->getGradeCollection()->getPayload();

        $this->assertEquals(
            [
                $this->grade1Payload
            ],
            $actualResult
        );
    }

    public function testGetGradeCollectionByCourseSectionGuidsWithStandardUser()
    {
        $this->request->method('getOptions')->willReturn([
            'courseSectionGuid' => [(string)$this->gradeData["courseSectionGuid"]],
            'uniqueId' => ['liaom'],
            'type' => ['midterm']

        ]);

        $this->apiUser->method('getUsername')->willReturn('liaom');
        $this->apiUser->method('checkAuthorization')->willReturn(null);
        $this->viewGradeService
            ->method('getCollectionByCourseSectionGuidsUniqueIdsTypes')
            ->willReturn(new GradeCollection([$this->grade1]));

        $this->viewInstructorAssignmentService
            ->method('getCollectionByCourseSectionGuidsUniqueIds')
            ->willReturn(new InstructorAssignmentCollection(
                [$this->instructorAssignment1]
            ));

        $actualResult = $this->gradeService->getGradeCollection()->getPayload();

        $this->assertEquals(
            [
                $this->grade1Payload
            ],
            $actualResult
        );
    }

    public function testGetGradeCollectionByTermCodesAndCrnsWithStandardUser()
    {
        $this->request->method('getOptions')->willReturn([
            'crn' => ['12345678'],
            'termCode' => ['201720'],
            'uniqueId' => ['liaom'],
            'type' => ['final']

        ]);

        $this->apiUser->method('getUsername')->willReturn('liaom');
        $this->apiUser->method('checkAuthorization')->willReturn(null);

        $this->viewGradeService
            ->method('getCollectionByTermCodesCrnsUniqueIdsTypes')
            ->willReturn(new GradeCollection([$this->grade1]));

        $this->viewInstructorAssignmentService
            ->method('getCollectionByTermCodesCrnsUniqeIds')
            ->willReturn(new InstructorAssignmentCollection(
                [$this->instructorAssignment1]
            ));

        $actualResult = $this->gradeService->getGradeCollection()->getPayload();

        $this->assertEquals(
            [
                $this->grade1Payload
            ],
            $actualResult
        );
    }

    public function testBodyRequestWithNoCourseSectionGuid()
    {
        $this->request->method('getData')->willReturn([
            [
                'uniqueId' => 'liaom',
                'gradeType' => 'midterm',
                'grade' => 'B'
            ]
        ]);
        $this->apiUser->method('getUsername')->willReturn('liaom');
        $reflectionGradeService = new ReflectionClass($this->gradeService);
        $reflectionUpdateGradeRequest = $reflectionGradeService->getMethod('constructUpdateGradeRequest');
        $reflectionUpdateGradeRequest->setAccessible(true);

        try {
            $reflectionUpdateGradeRequest->invoke($this->gradeService, $this->request->getData()[0], $this->apiUser->getUsername());
        } catch (\Exception $e) {
            $this->assertEquals('Missing courseSectionGuid', $e->getMessage());
        }
    }

    public function testBodyRequestWithNoUniqueId()
    {
        $this->request->method('getData')->willReturn([
            [
                'courseSectionGuid' => 'asdfgqwert',
                'gradeType' => 'midterm',
                'grade' => 'B'
            ]
        ]);
        $this->apiUser->method('getUsername')->willReturn('liaom');
        $reflectionGradeService = new ReflectionClass($this->gradeService);
        $reflectionUpdateGradeRequest = $reflectionGradeService->getMethod('constructUpdateGradeRequest');
        $reflectionUpdateGradeRequest->setAccessible(true);

        try {
            $reflectionUpdateGradeRequest->invoke($this->gradeService, $this->request->getData()[0], $this->apiUser->getUsername());
        } catch (\Exception $e) {
            $this->assertEquals('Missing uniqueId', $e->getMessage());
        }
    }

    public function testBodyRequestWithNoGradeType()
    {
        $this->request->method('getData')->willReturn([
            [
                'courseSectionGuid' => (string)$this->gradeData["courseSectionGuid"],
                'uniqueId' => 'liaom',
                'grade' => 'B'
            ]
        ]);
        $this->apiUser->method('getUsername')->willReturn('liaom');
        $reflectionGradeService = new ReflectionClass($this->gradeService);
        $reflectionUpdateGradeRequest = $reflectionGradeService->getMethod('constructUpdateGradeRequest');
        $reflectionUpdateGradeRequest->setAccessible(true);

        try {
            $reflectionUpdateGradeRequest->invoke($this->gradeService, $this->request->getData()[0], $this->apiUser->getUsername());
        } catch (\Exception $e) {
            $this->assertEquals('Missing gradeType', $e->getMessage());
        }
    }

    public function testBodyRequestWithNoGrade()
    {
        $this->request->method('getData')->willReturn([
            [
                'courseSectionGuid' => 'asdfgwqwert',
                'uniqueId' => 'liaom',
                'gradeType' => 'midterm'
            ]
        ]);
        $this->apiUser->method('getUsername')->willReturn('liaom');
        $reflectionGradeService = new ReflectionClass($this->gradeService);
        $reflectionUpdateGradeRequest = $reflectionGradeService->getMethod('constructUpdateGradeRequest');
        $reflectionUpdateGradeRequest->setAccessible(true);

        try {
            $reflectionUpdateGradeRequest->invoke($this->gradeService, $this->request->getData()[0], $this->apiUser->getUsername());
        } catch (\Exception $e) {
            $this->assertEquals('Missing grade', $e->getMessage());
        }
    }

    public function testNonAdminCannotImpersonateAnotherUser()
    {
        $this->request->method('getData')->willReturn([
            [
                'courseSectionGuid' => (string)$this->gradeData["courseSectionGuid"],
                'uniqueId' => 'liaom',
                'gradeType' => 'midterm',
                'grade' => 'B',
                'submittedBy' => 'unitueid2'
            ]
        ]);
        $this->apiUser->method('getUsername')->willReturn('liaom');
        $this->apiUser->method('checkAuthorization')->willReturn(null);

        $this->viewInstructorAssignmentService
            ->method('getCollectionByCourseSectionGuidsUniqueIds')
            ->willReturn(new InstructorAssignmentCollection(
                [$this->instructorAssignment1]
            ));
        $this->updateGradeService->method('bulkUpdate')->willReturn(new UpdateGradeResponseCollection());
        $actualResult = $this->gradeService->putGrade()->getPayload();

        $this->assertSame([
            [
                'isSuccessful' => false,
                'message' => 'You (liaom) are not authorized to impersonate user (unitueid2). Impersonation required admin privilege.'
            ]
        ], $actualResult);
    }

    public function testUseSubmittedByUserOverTokenUserIfPresent()
    {
        $this->request->method('getData')->willReturn([
            [
                'courseSectionGuid' => (string)$this->gradeData["courseSectionGuid"],
                'uniqueId' => 'liaom',
                'gradeType' => 'midterm',
                'grade' => 'B',
                'submittedBy' => 'unitueid2'
            ]
        ]);
        $this->apiUser->method('getUsername')->willReturn('liaom');
        $this->apiUser->method('checkAuthorization')->willReturn('admin');

        $this->viewInstructorAssignmentService
            ->method('getCollectionByCourseSectionGuidsUniqueIds')
            ->willReturn(new InstructorAssignmentCollection(
                [$this->instructorAssignment1]
            ));
        $this->updateGradeService
            ->expects($this->exactly(1))
            ->method('bulkUpdate')
            ->with($this->callback(function (UpdateGradeRequestCollection $requests) {
                if (count($requests) !== 1) {
                    return false;
                }

                if ($requests[0]->getInstructorUniqueId()->getValue() !== 'unitueid2') {
                    return false;
                }

                return true;
            }))
            ->willReturn(new UpdateGradeResponseCollection(
                [$this->gradePut1]
            ));
        $actualResult = $this->gradeService->putGrade()->getPayload();

        $this->assertEquals($this->gradePut1PayLoad, $actualResult);
    }

    public function testPutGradeWithAdminUser()
    {
        $this->request->method('getData')->willReturn([
            [
                'courseSectionGuid' => (string)$this->gradeData["courseSectionGuid"],
                'uniqueId' => 'liaom',
                'gradeType' => 'midterm',
                'grade' => 'B'
            ]
        ]);
        $this->apiUser->method('getUsername')->willReturn('liaom');
        $this->apiUser->method('checkAuthorization')->willReturn('admin');

        $this->viewInstructorAssignmentService
            ->method('getCollectionByCourseSectionGuidsUniqueIds')
            ->willReturn(new InstructorAssignmentCollection(
                [$this->instructorAssignment1]
            ));
        $this->updateGradeService
            ->method('bulkUpdate')
            ->willReturn(new UpdateGradeResponseCollection(
                [$this->gradePut1]
            ));
        $actualResult = $this->gradeService->putGrade()->getPayload();

        $this->assertEquals($this->gradePut1PayLoad, $actualResult);
    }

    public function testPutGradeWithNotAdminUser()
    {
        $this->request->method('getData')->willReturn([
            [
                'courseSectionGuid' => (string)$this->gradeData["courseSectionGuid"],
                'uniqueId' => 'liaom',
                'gradeType' => 'midterm',
                'grade' => 'B'
            ]
        ]);

        $this->apiUser->method('getUsername')->willReturn('xiaw');
        $this->apiUser->method('checkAuthorization')->willReturn(null);

        $this->viewInstructorAssignmentService
            ->method('getCollectionByCourseSectionGuidsUniqueIds')
            ->willReturn(new InstructorAssignmentCollection(
                []
            ));
        $this->updateGradeService
            ->method('bulkUpdate')
            ->willReturn(new UpdateGradeResponseCollection(
                [$this->gradePut2]
            ));

        $actualResult = $this->gradeService->putGrade()->getPayload();

        $this->assertEquals($this->gradePut2PayLoad, $actualResult);
    }
}
